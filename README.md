# База библиотек Altium Designer

**/Footprints**  --  Каталог с футпринтами компонентов, разбивка по категориям  
**/Symbols**  --  Условно-графические изображения компонентов  
**radiotech.DbLib**  --  Файл базы данных  
**radiotech_base.xls**  --  Файл БД в excel  
**/Templates**  --  Папка с шаблонами и форматками по ГОСТу  

**Author:** Anton Donets  
**Site:** https://radiotech.kz  
**Telegram chat:** https://t.me/radiotechkz  
**Email:** info@radiotech.kz  
